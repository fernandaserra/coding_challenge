#include<iostream>
#include <iomanip>
using namespace std;

int main()
{
    float a, b, s = 0;
    cin>>a; cin>>b;
     
    s = a/b;
    cout << fixed;
    cout << setprecision(2);
    cout<<s<<"\n";

    return 0;
}